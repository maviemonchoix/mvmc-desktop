import Vue from 'vue'
import 'bootstrap'
import Router from 'vue-router'
import Home from './views/Home.vue'
import orientation from './views/Orientation.vue'
import Carte from './views/Carte.vue'
import Faq from './views/Faq.vue'
import Glossaire from './views/Glossaire.vue'
import Parent from './views/Parent.vue'
import LessonsIndex from './views/lessons/Index.vue'
import LessonsShow from './views/lessons/Show.vue'

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      name: 'home',
      path: '/',
      component: Home
    },
    {
      path: '/main',
      component: LessonsIndex,
      children: [
        {
          name: 'carte',
          path: 'carte',
          component: Carte
        },
        {
          name: 'faq',
          path: '/faq',
          component: Faq
        },
        {
          name: 'glossaire',
          path: '/glossaire',
          component: Glossaire
        },
        {
          name: 'parent',
          path: '/parent',
          component: Parent
        },
        {
          name: 'orientation',
          path: '/orientation',
          component: orientation
        }
      ]
    },
    {
      name: 'lessons',
      path: '/lessons',
      component: LessonsIndex,
      beforeEnter (to, from, next) {
        let route = true

        if (to.name === 'lessons') {
          route = {
            name: 'lesson',
            params: {
              id: 1,
              view: 'introduction'
            }
          }
        }

        next(route)
      },
      children: [
        {
          name: 'lesson',
          path: ':id/:view',
          component: LessonsShow,
          beforeEnter (to, from, next) {
            let route = true

            if (!to.params.view) {
              route = to
              route.params.view = 'introduction'
            }

            next(route)
          }
        }
      ]
    }
  ]
})
