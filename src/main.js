import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import fontawesome from '../node_modules/@fortawesome/fontawesome'
import { faBars, faBook, faCheck, faChevronRight, faCircle, faComments, faHandPaper, faLaptop, faList, faPlay }
  from '../node_modules/@fortawesome/fontawesome-free-solid'

fontawesome.library.add(faBars, faBook, faCheck, faChevronRight, faCircle, faComments, faLaptop, faHandPaper, faList, faPlay)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
